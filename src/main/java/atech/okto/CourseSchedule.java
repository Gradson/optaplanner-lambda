package atech.okto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@PlanningSolution
@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseSchedule {
    private List<Integer> roomList;
    private List<Integer> periodList;
    private List<Lecture> lectureList;
    private HardSoftScore score;

    @PlanningEntityCollectionProperty
    public List<Lecture> getLectureList() {
        return lectureList;
    }

    @ValueRangeProvider(id = "availableRooms")
    @ProblemFactCollectionProperty
    public List<Integer> getRoomList() {
        return roomList;
    }

    @ValueRangeProvider(id = "availablePeriods")
    @ProblemFactCollectionProperty
    public List<Integer> getPeriodList() {
        return periodList;
    }

    @PlanningScore
    public HardSoftScore getScore() {
        return score;
    }


    public CourseSchedule solve(Map<String, Object> input) throws IOException {
        final String json = input.getOrDefault("body", new CourseSchedule()).toString();
        final CourseSchedule courseSchedule = new ObjectMapper().readValue(json, CourseSchedule.class);

        SolverFactory<CourseSchedule> solverFactory = SolverFactory.createFromXmlResource("opta/courseScheduleSolverConfiguration.xml");
        Solver<CourseSchedule> solver = solverFactory.buildSolver();
        return solver.solve(courseSchedule);
    }
}
