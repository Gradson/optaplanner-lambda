package com.serverless;

import atech.okto.CourseSchedule;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

public class Opta implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

	@Override
	public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
		try {
			CourseSchedule solvedCourseSchedule = new CourseSchedule();

			return ApiGatewayResponse.builder()
					.setStatusCode(200)
					.setObjectBody(solvedCourseSchedule.solve(input))
					.setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & serverless"))
					.build();
		} catch (IOException e) {
			e.printStackTrace();
			return ApiGatewayResponse.builder()
					.setStatusCode(500).build();
		}
	}
}
